TokenList InitTokenList(){
    TokenList list = {};
    list.array_size = INITIAL_TOKEN_LIST_SIZE;
    //Allocate memory for tokens array
    list.tokens = (Token *)calloc(list.array_size, sizeof(Token)); //Initial token array
    return list;
}

void AddToken(TokenList *list, Token token){
	//If token array is full
	if(list->token_count >= list->array_size){
		//Increase array size by 10
		list->array_size += 10;
		//Realloc array
		list->tokens = (Token *)realloc(list->tokens, sizeof(Token) * list->array_size);
	}
	//Add current token into array
	list->tokens[list->token_count] = token;
	list->token_count++;
}

/* Pop the token at the top of the array, decrement the pointer and return the popped token */
Token* PopTokenList(TokenList *list){
    if(list->token_count > -1){
        list->token_count--;
        return &(list->tokens[list->token_count]);
    }else{
        printf("Could not pop from token list\n");
        exit(-1);
    }
}


void GenTokens(FileInfo *file_info, TokenList *list){
    char *source = file_info->data;
	char *token_string;
    while((token_string=NextChar(file_info, source)) != NULL){
        Token token;
		token.line = line;
		token.start = current_char - 1;
		token.end = current_char -1;
		switch(*token_string){
			case '(':{
                token.data = "(";
                token.type = separator;
				AddToken(list, token);
				break;
            }
			case ')':{
				token.data = ")";
                token.type = separator;
				AddToken(list, token);
                
				break;
            }
			case '{':{
				token.data = "{";
                token.type = separator;
				AddToken(list, token);
				break;
            }
			case '}':{
				token.data = "}";
                token.type = separator;
				AddToken(list, token);
				break;
            }
			case '[':{
				token.data = "[";
                token.type = separator;
				AddToken(list, token);
                
				break;
            }
            case ']':{
				token.data = "]";
                token.type = separator;
                AddToken(list, token);
				break;
            }
			case ';':{
				token.data = ";";
                token.type = separator;
				AddToken(list, token);
				break;
            }
			case '.':{
				token.data = ".";
                token.type = separator;
				AddToken(list, token);
				break;
            }
			case ',':{
				token.data = ",";
                token.type = separator;
				AddToken(list, token);
				break;
            }
			case '+':{
				token.data = "+";
                token.type = oper;
				AddToken(list, token);
				break;
            }
			case '-':{
				token.data = "-";
                token.type = oper;
				AddToken(list, token);
                
				break;
            }
			case '*':{
				token.data = "*";
                token.type = oper;
				AddToken(list, token);
                
				break;
            }
			case '/':{
                token.data = "/";
                token.type = oper;
				AddToken(list, token);
				break;
            }
            case '!':{
                char *next = PeekChar(file_info, source);
                if(*next == '='){
					token.end=current_char;
                    //only increment current char if we get a '=' operator
                    next = NextChar(file_info, source);
                    token.data = "!=";
                    token.type = oper;
                    AddToken(list, token);
                }else{
                    token.data = "!";
                    token.type = oper;
                    AddToken(list, token);
                }
                break;
            }
            case '=':{
                char *next = PeekChar(file_info, source);
                if(*next == '='){
					token.end=current_char;
                    next = NextChar(file_info, source);
                    //only increment current char if we get a '=' operator
                    next = NextChar(file_info, source);
                    token.data = "==";
                    token.type = oper;
                    AddToken(list, token);
                }else{
                    token.data = "=";
                    token.type = oper;
                    AddToken(list, token);
                }
                break;
            }
            case '<':{
                char *next = PeekChar(file_info, source);
                if(*next == '='){
					token.end=current_char;
                    next = NextChar(file_info, source);
                    //only increment current char if we get a '=' operator
                    next = NextChar(file_info, source);
                    token.data = "<=";
                    token.type = oper;
                    AddToken(list, token);
                }else{
                    token.data = "<";
                    token.type = oper;
                    AddToken(list, token);
                }
                break;
            }
            case '>':{
                char *next = PeekChar(file_info, source);
                if(*next == '='){
					token.end=current_char;
                    next = NextChar(file_info, source);
                    //only increment current char if we get a '=' operator
                    next = NextChar(file_info, source);
                    token.data = ">=";
                    token.type = oper;
                    AddToken(list, token);
                }else{
                    token.data = ">";
                    token.type = oper;
                    AddToken(list, token);
                }
                break;
            }
			case '\n':{
				token.data = "\n";
				token.type = newline;
				AddToken(list, token);
                line++;
                break;
            }
        }
		if(isalpha(*token_string)){
            int char_count = 0;
            //Create char array big enough for 1 char and string terminator
            char *string = (char *)malloc(sizeof(char)*2);
            //Put first char in string
            string[0] = *token_string;
            //Peek the next char
            char *next = PeekChar(file_info, source);
            //
            while(isalnum(*next)){
                char_count++;
                //Get next character
                token_string = NextChar(file_info,source);
                //Increase the size of array
                string = (char *)realloc(string, sizeof(char) * char_count);
                //Put next char in string
                string[char_count] = *token_string;
                //Peek the next char
                next = PeekChar(file_info, source);
            }
            token.end = current_char - 1;
            //Terminate string
            string[char_count+1] = '\0';
            token.data = string;
            if(KeywordCheck(string)){
                token.type = keyword;
            }
            else{
                token.type = identifier;
            }
            AddToken(list, token);
		}
		else if(*token_string >= '1' && *token_string <= '9'){
			int char_count = 0;
			int decimal_point = 0;
            char *string = (char *)malloc(sizeof(char)*2);
            //Put first char in string
            string[0] = *token_string;
            //Peek the next char
            char *next = PeekChar(file_info, source);
			while((*next >= '0' && *next <='9') || (*next=='.' && !decimal_point)){
				char_count++;
				if(*next=='.'){
                    decimal_point = 1;
				}
                token_string = NextChar(file_info,source);
                //Increase the size of array
                string = (char *)realloc(string, sizeof(char) * char_count+1);
                //Put next char in string
                string[char_count] = *token_string;
                //Peek the next char
                next = PeekChar(file_info, source);
            }
            token.end = current_char - 1;
            string = (char *)realloc(string, sizeof(char) * char_count+2);
            string[char_count+1] = '\0';
			token.data = string;
			token.type = literal;
			AddToken(list,token);
        }
	}
}

void PrintTokens(TokenList token_list){
    for(int i = 0; i < token_list.token_count; i++){
        const char *type;
        switch(token_list.tokens[i].type){
            case(oper):{
                type = "Operator";
                break;
            }
            case(literal):{
                type = "Literal";
                break;
            }
            case(separator):{
                type = "Separator";
                break;
            }
            case(identifier):{
                type = "Identifier";
                break;
            }
            case(keyword):{
                type = "Keyword";
                break;
            }
            case(comment):{
                type = "Comment";
                break;
            }
            case(newline):{
                type = "Newline";
                break;
            }
            default:{
                type = "Unknown";
                break;
            }
        }
        printf("Token %i:\n    Value: %s\n    Type: %s\n    Line:%u\n    Start:%u\n    End:%u\n", i, token_list.tokens[i].data, type,token_list.tokens[i].line, token_list.tokens[i].start, token_list.tokens[i].end);
    }
}
