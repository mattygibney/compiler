i32 GetOperatorType(Token token){
    if(strcmp(token.data, "+") == 0){
        return addition;
    }
    else if(strcmp(token.data, "*") == 0){
        return multiplication;
    }
    else if(strcmp(token.data, "/") == 0){
        return division;
    }
    else if(strcmp(token.data, "-") == 0){
        return subtraction;
    }
    else if(strcmp(token.data, "(") == 0){
        return parenthesis;
    }
    else if(strcmp(token.data, ")") == 0){
        return parenthesis;
    }
    else{
        printf("Error: Invalid Operator Type.\n");
        exit(-1);
    }
}

/* Returns: -1 if first is lower precedence,
       0 if same precedence and 1 if first is higher precedence
*/
i32 CompareOperatorPrecedence(u32 first, u32 second){
    if(first < second) return -1;
    else if(first > second) return 1;
    else return 0;
}

ParserInfo ParseTokens(TokenList *token_list){
    ParserInfo parser_info = {};
    parser_info.operator_stack = InitTokenList();
    parser_info.output_queue = InitTokenList();
    
    /* Parse tokens using shunting yard algorithm
https://en.wikipedia.org/wiki/Shunting-yard_algorithm - used the pseudocode from 'the algorithm in detail' part
Currently just parses simple maths equations */
    
    for(int i = 0; i < token_list->token_count; i++){
        if(token_list->tokens[i].type == oper){
            if (parser_info.operator_stack.token_count == 0){
                AddToken(&parser_info.operator_stack, token_list->tokens[i]);
            }
            else{
                //get parser_info.operator token on top of stack and compare precedence with parser_info.operator being added
                Token top_token = parser_info.operator_stack.tokens[parser_info.operator_stack.token_count - 1];
                i32 top_token_type = GetOperatorType(top_token);
                i32 token_being_added_type = GetOperatorType(token_list->tokens[i]);
                //while the parser_info.operator stack top value is a higher precedence than the token being added, pop it and add it to the parser_info.output queue
                while((parser_info.operator_stack.token_count != 0) && (CompareOperatorPrecedence(top_token_type, token_being_added_type) >= 0) && (strcmp(parser_info.operator_stack.tokens[parser_info.operator_stack.token_count - 1].data, "(") != 0)){
                    Token *operator_token = PopTokenList(&parser_info.operator_stack);
                    top_token = parser_info.operator_stack.tokens[parser_info.operator_stack.token_count];
                    u32 top_token_type = GetOperatorType(top_token);
                    AddToken(&parser_info.output_queue, *operator_token);
                }
                //add parser_info.operator to stack
                AddToken(&parser_info.operator_stack, token_list->tokens[i]);
            }
        }
		else if(token_list->tokens[i].type == identifier || token_list->tokens[i].type == literal){
			AddToken(&parser_info.output_queue, token_list->tokens[i]);
		}
        else if(token_list->tokens[i].type == separator){
            //strmp returns 0 when the strings are equal
            if(strcmp(token_list->tokens[i].data, "(") == 0){
                AddToken(&parser_info.operator_stack, token_list->tokens[i]);
            }
            else if(strcmp(token_list->tokens[i].data, ")") == 0){
                while(strcmp(parser_info.operator_stack.tokens[parser_info.operator_stack.token_count-1].data, "(") != 0){
                    Token *operator_token = PopTokenList(&parser_info.operator_stack);
                    AddToken(&parser_info.output_queue, *operator_token);
                }
                /* If the stack runs out without finding a left parenthesis, then there are mismatched parentheses. */
                if(strcmp(parser_info.operator_stack.tokens[parser_info.operator_stack.token_count-1].data, "(") == 0){
                    PopTokenList(&parser_info.operator_stack); //discard the left parenthesis
                }
            }
        }
        else if(token_list->tokens[i].type==newline){
            while((parser_info.operator_stack.token_count != 0)){
                Token *operator_token = PopTokenList(&parser_info.operator_stack);
                AddToken(&parser_info.output_queue, *operator_token);
            }
            AddToken(&parser_info.output_queue, token_list->tokens[i]);
        }
    }
    //if there are any parser_info.operators left in the stack add it to the queue
    while((parser_info.operator_stack.token_count != 0)){
        Token *operator_token = PopTokenList(&parser_info.operator_stack);
        AddToken(&parser_info.output_queue, *operator_token);
    }
    return parser_info;
}

ASTNode* CreateASTNode(Token *token,int max_children){
    ASTNode *node = (ASTNode*)malloc(sizeof(ASTNode));
    //Store the token
    node->token= *token;
    //Create the dynamic array for children
    node->children = (ASTNode **)malloc(sizeof(ASTNode*));
    //Initially it has no children
    node->num_children = 0;
    //Set the max children
    node->max_children = max_children;
    return node;
}


u32 NodeHasSpace(ASTNode *node){
    //Check if the max number of children have been reached or it has no max
    if(node->max_children>node->num_children || node->max_children == -1){
        return 1;
    }
    else{
        return 0;
    }
}

//returns 0 if node was added successfully, returns 1 if a node couldn't be added because all the nodes children are taken.
void InsertASTNode(ASTNode *node, ASTNode *parent){
    //Node has space for another child
    if(NodeHasSpace(parent)){
        //Increase the child counter
        parent->num_children++;
        //Increase the size of children array by 1
        parent->children=(ASTNode**)realloc(parent->children,(parent->num_children)*sizeof(ASTNode*));
        //Add child to correct position in child array
        parent->children[parent->num_children-1] = node;
        //Set nodes parent
        node->parent=parent;
    }
    //Node is already full
    else{
        printf("Node is already full ERROR");
        exit(1);
    }
}


AST* CreateAST(ParserInfo *parser_info) {
    AST *ast = (AST *) malloc(sizeof(AST ));

    //Create the root node of the ast
    ASTNode *root_node = (ASTNode *)malloc(sizeof(ASTNode));
    //Create a placeholder token so we can see its the root node
    Token *temp = (Token*)malloc(sizeof(Token));
    temp->data = "ROOT NODE OF TREE";

    //We set max_children to -1 when it can have infinite children temporarily
    root_node->max_children = -1;
    root_node->token = *temp;
    root_node->parent=NULL;
    root_node->num_children = 0;
    root_node->children=(ASTNode**)malloc(sizeof(ASTNode*)*1);

    //Set the ast root node to the node we have just created
    ast->root_node=root_node;

    //Token and node to use each loop
    Token *current_token;
    ASTNode *current_node;

    //Whilst we still have tokens in the queue
    while (parser_info->output_queue.token_count > 0) {
        current_token = PopTokenList(&(parser_info->output_queue));
        current_node = (ASTNode *) malloc(sizeof(ASTNode));
        //Adding to root Node of tree
        if (root_node->parent == NULL) {
            if (current_token->type == oper) {
                //Create token that can have two children
                current_node = CreateASTNode(current_token, 2);
                InsertASTNode(current_node, root_node);
                root_node = current_node;
            }
            else if (current_token->type == newline) {
                //Go back up to root node or last keyword node when implemented
                while (root_node->parent != NULL) {
                    root_node = root_node->parent;
                }
            } else {
                printf("ERROR adding literal to root node\n");
            }
        } else {
            if (current_token->type == literal) {
                current_node = CreateASTNode(current_token, 0);
                //Add to the node above if space
                if (NodeHasSpace(root_node)) {
                    InsertASTNode(current_node, root_node);
                }
                    //keep traversing the tree until you find a suitable parent to add to
                else {
                    while ((root_node->parent != NULL)) {
                        root_node = root_node->parent;
                        if (NodeHasSpace(root_node)) {
                            InsertASTNode(current_node, root_node);
                            break;
                        }
                    }
                }

            } else if (current_token->type == oper) {
                current_node = CreateASTNode(current_token, 2);
                if (NodeHasSpace(root_node)) {
                    InsertASTNode(current_node, root_node);
                    root_node = current_node;
                } else {
                    while ((root_node->parent != NULL)) {
                        root_node = root_node->parent;
                        if (NodeHasSpace(root_node)) {
                            InsertASTNode(current_node, root_node);
                            break;
                        }
                    }
                    root_node = current_node;
                }
            }
        }
    }
    return ast;

}