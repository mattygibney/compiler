#ifndef PARSER_H
#define PARSER_H

/* Node for the abstract syntax tree */
struct ASTNode{
    Token token;
    ASTNode *parent;
    ASTNode **children;
    i32 max_children;
    u32 num_children;
};

struct AST{
    ASTNode *root_node;
};

struct ParserInfo{
    TokenList operator_stack;
    TokenList output_queue;
};

enum{
    parenthesis = 0,
    subtraction = 1,
    addition = 1,
    multiplication = 2,
    division = 2
};
#endif //PARSER_H
