#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "types.h"
#include "fileinfo.h"
#include "lexer.h"
#include "parser.h"

#include "fileinfo.cpp"
#include "lexer.cpp"
#include "parser.cpp"

/*
Precedence (high to low):
Brackets
Powers(not implemented in lexer yet)
Division/Multiplication
Addition/Subtraction
*/



int main(){
    FileInfo file_info = ReadFileToString("code/test.lang");
    TokenList token_list = InitTokenList();
    GenTokens(&file_info, &token_list);
    FreeFileString(&file_info);
    ParserInfo parser_info = ParseTokens(&token_list);
    AST *ast = CreateAST(&parser_info);

    printf("\n\n%s\n",ast->root_node->children[0]->token.data);
    printf("%s\n",ast->root_node->children[0]->children[0]->token.data);
    printf("%s\n",ast->root_node->children[0]->children[1]->token.data);
    printf("\n\n%s\n",ast->root_node->children[1]->token.data);
    printf("%s\n",ast->root_node->children[1]->children[0]->token.data);
    printf("%s\n",ast->root_node->children[1]->children[1]->token.data);
    printf("\n\n%s\n",ast->root_node->children[2]->token.data);
    printf("%s\n",ast->root_node->children[2]->children[0]->token.data);
    printf("%s\n",ast->root_node->children[2]->children[1]->token.data);
    return 0;
}

