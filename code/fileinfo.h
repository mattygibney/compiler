#ifndef FILEINFO_H
#define FILEINFO_H

//Todo: maybe pull these variables into a main 'lexer' struct?
u32 line=0; //Keep track of the current line number we are on
u32 current_char=0; //current char index
u32 start_char=0; //index at start of token

const char *keywords[] = {"if","else","int", "while"};
const u32 keyword_length = sizeof(keywords)/sizeof(keywords[0]);

struct FileInfo{
    char *data;
    u32 size;
};

#endif //FILEINFO_H
