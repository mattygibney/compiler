FileInfo ReadFileToString(const char *filename){
    FileInfo file_info = {};
    FILE *f = fopen(filename, "r");
    if(!f){
        printf("Error: Could not open source file.\n");
        fclose(f);
        exit(-1);
    }else{
        /* Get the length of the file, then read that many bytes into the string. */
        fseek(f, 0, SEEK_END);
        file_info.size = ftell(f);
        fseek(f, 0, SEEK_SET);
        file_info.data = (char *)malloc(file_info.size + 1);
        fread(file_info.data, file_info.size, 1, f);
        fclose(f);
        file_info.data[file_info.size] = '\0'; //string null terminator
        return file_info;
    }
}

void FreeFileString(FileInfo *file_info){
    free(file_info->data);
    file_info->size = 0;
}

int EndOfFile(FileInfo *file_info, u32 position){
	//Return 1 if at the end of the file and 0 if not
	if(position > (file_info->size)){
		return 1;
	}
	else {
		return 0;
	}
}

char* NextChar(FileInfo *file_info, char *source){
	if(!EndOfFile(file_info, current_char)){
		char *c = &source[current_char];
		current_char++;
		return c;
	}
	else{
		return NULL;
	}
}

/* Looks at the next char, but doesn't increment the current char counter */
char *PeekChar(FileInfo *file_info, char *source){
    if(!EndOfFile(file_info, current_char)){
        char *c = &source[current_char];
        return c;
    }
    else{
        return NULL;
    }
}

int KeywordCheck(char *string){
	for(int i=0; i<keyword_length; i++){
		if(!strcmp(string,keywords[i])){
			return 1;
		}
	}
	return 0;
}
