#ifndef LEXER_H
#define LEXER_H

enum TokenType { identifier, keyword, separator, oper, literal, comment,newline }; //All the different token types

struct Token{
    u32 line;
	const char *data;
    u32 start;
    u32 end;
	TokenType type;
};

struct TokenList{
    Token *tokens;
    u32 array_size;
    i32 token_count;
    u32 front;
};

#endif //LEXER_H
